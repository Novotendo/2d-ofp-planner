#include <frontiers_filter.h>

int main(int argc, char** argv){
    ros::init(argc, argv, "frontiers_filter_node");
    ros::NodeHandle nh_("");
    ros::NodeHandle nh_private_("~");
    ros::Rate loop_rate(40);


    FRONTIERS_FILTER clustering_filter(nh_, nh_private_);
    ROS_INFO("Initialized Frontiers Filter node.");
    clustering_filter.init();

    /*while(ros::ok()){
        ros::spinOnce();

        loop_rate.sleep();
    }*/

    //ros::spin();

    return 0;
}