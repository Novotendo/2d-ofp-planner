#include <ofp_assigner.h>

OFP_ASSIGNER::OFP_ASSIGNER(const ros::NodeHandle& nh, const ros::NodeHandle& nh_private)
    : nh_(nh),
      nh_private_(nh_private){

    nh_private_.param("robot_radius", robot_radius, robot_radius);
	nh_private_.param("resolution", resolution, resolution);
	nh_private_.param("information_radius", information_radius, information_radius);
	nh_private_.param("free_th", free_th, free_th);
	nh_private_.param("lambda_path_cost", lambda_path_cost, lambda_path_cost);

	robot_pose_sub = nh_private_.subscribe("/odom", 1, &OFP_ASSIGNER::robot_pose_callback, this);
	map_sub = nh_private_.subscribe("/map", 1, &OFP_ASSIGNER::map_callback, this);
	filtered_frontiers_points_sub = nh_private_.subscribe("/filtered_frontier_points", 1, &OFP_ASSIGNER::frontiers_filter_callback, this);

	rviz_optimal_frontier_point_pub = nh_private_.advertise<visualization_msgs::Marker>("/rviz/optimal_frontier_point", 1);
	optimal_frontier_point_pub = nh_private_.advertise<geometry_msgs::Point>("/optimal_frontier_point", 1);
	finish_exploration_pub = nh_private_.advertise<std_msgs::Bool>("/finish_fancy_exploration", 1, true);
}




void OFP_ASSIGNER::robot_pose_callback(const geometry_msgs::PoseStamped::ConstPtr& msg){
	robot_pose.x() = msg -> pose.position.x; robot_pose.y() = msg -> pose.position.y; robot_pose.z() = msg -> pose.position.z;
}




void OFP_ASSIGNER::map_callback(const nav_msgs::OccupancyGrid::ConstPtr& msg){
    mapData = *msg;
	map_width = mapData.info.width * mapData.info.resolution;
	map_height = mapData.info.height * mapData.info.resolution;
	map_x_init = mapData.info.origin.position.x;
	map_y_init = mapData.info.origin.position.y;
	map_resolution = mapData.info.resolution;
}




void OFP_ASSIGNER::frontiers_filter_callback(const ofp_planner::point_array::ConstPtr& msg){
	Eigen::Vector3d ofp_candidate;
	ofp_candidates_list.clear();

	for(auto ptr = msg -> points.begin() ; ptr != msg -> points.end() ; ++ptr){
		ofp_candidate.x() = ptr -> x; ofp_candidate.y() = ptr -> y; ofp_candidate.z() = ptr -> z; 
		ofp_candidates_list.push_back(ofp_candidate);
	}
}




unsigned int OFP_ASSIGNER::get_index_of_point(const Eigen::Vector3d& point) const{
	Eigen::Vector2d fixed_map_point;
	fixed_map_point.x() = point.x() - map_x_init;
	fixed_map_point.y() = point.y() - map_y_init;
	return (int)(floor(fixed_map_point.y() / map_resolution) * mapData.info.width + floor(fixed_map_point.x() / map_resolution));
}




Eigen::Vector3d OFP_ASSIGNER::get_point_of_index(const unsigned int& index) const{
	Eigen::Vector3d point;
	point.x() = map_resolution * (index - mapData.info.width * floor(index / mapData.info.width) + 0.5) + map_x_init;
	point.y() = map_resolution * (floor(index / mapData.info.width) + 0.5) + map_y_init;
	point.z() = 0.0;
	return point;
}




double OFP_ASSIGNER::info_in_area(const Eigen::Vector3d& center, double& radius) const{
	unsigned int index_center, fixed_radius, index_init, search_index;
	double info_gain;

	index_center = get_index_of_point(center);
	fixed_radius = (int)(radius / map_resolution);
	index_init = index_center - fixed_radius * (mapData.info.width + 1);
	info_gain = 0.0;

	for(unsigned int i = 0 ; i < (2 * fixed_radius + 1) ; i++){
		for(unsigned int j = 0 ; j < (2 * fixed_radius + 1) ; j++){
			search_index = index_init + i + j * mapData.info.width;
			if(mapData.data[search_index] == -1){
				info_gain++;
			}

			if(search_index % mapData.info.width == 0){ // If index is at limit of X, break so there is no next index error
				break;
			}
		}

		if(search_index + 1 >= mapData.data.size()){ // If index is at limit of Y, break so there is no next index error
			break;
		}
	}

	return info_gain / pow(2 * fixed_radius + 1, 2.0);
}




space_state OFP_ASSIGNER::check_space_state(const Eigen::Vector3d& position) const{
	unsigned int index_center, fixed_radius, index_init, search_index;
	bool unexplored;
	int8_t cell_data;
	index_center = get_index_of_point(position);
	fixed_radius = (int)(robot_radius / map_resolution);
	index_init = index_center - fixed_radius * (mapData.info.width + 1);

	unexplored = false;

	for(unsigned int i = 0 ; i < (2 * fixed_radius + 1) ; i++){
		for(unsigned int j = 0 ; j < (2 * fixed_radius + 1) ; j++){
			search_index = index_init + i + j * mapData.info.width;
			cell_data = mapData.data[search_index];
			if(cell_data > free_th){
				return OCCUPIED;
			}
			else if(cell_data == -1){
				unexplored = true;
			}

			if(search_index % mapData.info.width == 0){ // If index is at limit of X, break so there is no next index error
				break;
			}
		}

		if(search_index + 1 >= mapData.data.size()){ // If index is at limit of Y, break so there is no next index error
			break;
		}
	}

	if(unexplored){
		return UNEXPLORED;
	}
	else{
		return FREE;
	}
}




void OFP_ASSIGNER::rviz_show_ofp(){
    visualization_msgs::Marker optimal_frontier_point_marker;
	geometry_msgs::Point position;

    optimal_frontier_point_marker.header.frame_id = "world";
    optimal_frontier_point_marker.header.stamp = ros::Time();
    optimal_frontier_point_marker.ns = "optimal_frontier point";

	if(ofp_candidates_list.size() > 0){
    	optimal_frontier_point_marker.action = visualization_msgs::Marker::ADD;
	}

	else{
		optimal_frontier_point_marker.action = visualization_msgs::Marker::DELETEALL;
	}

    optimal_frontier_point_marker.id = 3;
    optimal_frontier_point_marker.lifetime = ros::Duration();
    optimal_frontier_point_marker.type = visualization_msgs::Marker::SPHERE_LIST;
    optimal_frontier_point_marker.scale.x = optimal_frontier_point_marker.scale.y = optimal_frontier_point_marker.scale.z = 3*map_resolution;
    optimal_frontier_point_marker.color.a = 1.0;
    optimal_frontier_point_marker.color.r = 0.0;
    optimal_frontier_point_marker.color.g = 1.0;
    optimal_frontier_point_marker.color.b = 0.0;
    optimal_frontier_point_marker.pose.orientation.w = 1.0;

	position.x = optimal_frontier_point.position.x();
	position.y = optimal_frontier_point.position.y();
	position.z = optimal_frontier_point.position.z();
	optimal_frontier_point_marker.points.push_back(position);

	rviz_optimal_frontier_point_pub.publish(optimal_frontier_point_marker);
}




bool compareByGain(OFP* const & a, OFP* const & b){
    return a->gain > b->gain;
}




void OFP_ASSIGNER::compute_ofp(OFP& optimal_frontier_point){
	double frontier_point_info, frontier_point_euclidean, frontier_gain;
	vector<OFP*> frontier_buffer_selection;
	geometry_msgs::Point OFP_msg;
	std_msgs::Bool the_end;

	if(ofp_candidates_list.size() > 0){
		the_end.data = false;
		frontier_buffer_selection.clear();
		for(auto ptr = ofp_candidates_list.begin() ; ptr != ofp_candidates_list.end() ; ++ptr){
			frontier_point_euclidean = get_point2point_distance(robot_pose, *ptr);
			if(frontier_point_euclidean > robot_radius){
				frontier_point_info = info_in_area(*ptr, information_radius);
				frontier_gain = frontier_point_info + lambda_path_cost/frontier_point_euclidean;
				frontier_buffer_selection.push_back(new OFP{*ptr, frontier_gain});
			}
		}	
		if(frontier_buffer_selection.size() > 0){
			sort(frontier_buffer_selection.begin(), frontier_buffer_selection.end(), compareByGain);
			optimal_frontier_point = *frontier_buffer_selection.front();
			OFP_msg.x = optimal_frontier_point.position.x(); OFP_msg.y = optimal_frontier_point.position.y(); OFP_msg.z = optimal_frontier_point.position.z();
			optimal_frontier_point_pub.publish(OFP_msg);
		}
		wall_reset_begin = ros::WallTime::now();
	}

	else{
		wall_reset_now = ros::WallTime::now();
		wall_reset_duration = wall_reset_now - wall_reset_begin;
		if(wall_reset_duration.sec > 2.0){ // Wait a bit more for new possible frontiers
			the_end.data = true;
		}
	}

	rviz_show_ofp();
	finish_exploration_pub.publish(the_end);
}


 


void OFP_ASSIGNER::init(){
	/*ros::WallDuration wall_duration;
	ros::WallTime wall_begin, wall_now;*/

	while(ros::ok() && ofp_candidates_list.size() == 0){ros::spinOnce();} // Wait for first frontiers before init

	while(ros::ok()){
		//wall_begin = ros::WallTime::now();
		ros::spinOnce();	
		compute_ofp(optimal_frontier_point);
		/*wall_now = ros::WallTime::now();
		wall_duration = wall_now - wall_begin;
		ROS_WARN("WALL OFP ASSIGNER: %u.%09u s", wall_duration.sec, wall_duration.nsec);*/
	}
}