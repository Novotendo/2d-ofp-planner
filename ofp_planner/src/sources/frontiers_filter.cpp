#include <frontiers_filter.h>

FRONTIERS_FILTER::FRONTIERS_FILTER(const ros::NodeHandle& nh, const ros::NodeHandle& nh_private)
    : nh_(nh),
      nh_private_(nh_private){

    nh_private_.param("robot_radius", robot_radius, robot_radius);
	nh_private_.param("resolution", resolution, resolution);
	nh_private_.param("clustering_radius", clustering_radius, clustering_radius);
	nh_private_.param("free_th", free_th, free_th);

	map_sub = nh_private_.subscribe("/map", 10, &FRONTIERS_FILTER::map_callback, this);
	global_frontiers_points_sub = nh_private_.subscribe("/global_frontier_points", 10, &FRONTIERS_FILTER::global_frontier_callback, this);

	rviz_filtered_frontier_points_pub = nh_private_.advertise<visualization_msgs::Marker>("/rviz/filtered_frontier_points", 1);
	filtered_frontier_points_pub = nh_private_.advertise<ofp_planner::point_array>("/filtered_frontier_points", 1);
}




void FRONTIERS_FILTER::map_callback(const nav_msgs::OccupancyGrid::ConstPtr& msg){
    mapData = *msg;
	map_width = mapData.info.width * mapData.info.resolution;
	map_height = mapData.info.height * mapData.info.resolution;
	map_x_init = mapData.info.origin.position.x;
	map_y_init = mapData.info.origin.position.y;
	map_resolution = mapData.info.resolution;
}




void FRONTIERS_FILTER::global_frontier_callback(const geometry_msgs::Point::ConstPtr& msg){
	new_global_frontier_point.x() = msg -> x; new_global_frontier_point.y() = msg -> y; new_global_frontier_point.z() = msg -> z;
	clustering_filter(new_global_frontier_point);
}




unsigned int FRONTIERS_FILTER::get_index_of_point(const Eigen::Vector3d& point) const{
	Eigen::Vector2d fixed_map_point;
	fixed_map_point.x() = point.x() - map_x_init;
	fixed_map_point.y() = point.y() - map_y_init;
	return (int)(floor(fixed_map_point.y() / map_resolution) * mapData.info.width + floor(fixed_map_point.x() / map_resolution));
}




Eigen::Vector3d FRONTIERS_FILTER::get_point_of_index(const unsigned int& index) const{
	Eigen::Vector3d point;
	point.x() = map_resolution * (index - mapData.info.width * floor(index / mapData.info.width) + 0.5) + map_x_init;
	point.y() = map_resolution * (floor(index / mapData.info.width) + 0.5) + map_y_init;
	point.z() = 0.0;
	return point;
}




space_state FRONTIERS_FILTER::check_space_state(const Eigen::Vector3d& position, const double& radius) const{
	unsigned int index_center, fixed_radius, index_init, search_index;
	bool unexplored;
	int8_t cell_data;
	index_center = get_index_of_point(position);
	fixed_radius = (int)(radius / map_resolution);
	index_init = index_center - fixed_radius * (mapData.info.width + 1);

	unexplored = false;

	for(unsigned int i = 0 ; i < (2 * fixed_radius + 1) ; i++){
		for(unsigned int j = 0 ; j < (2 * fixed_radius + 1) ; j++){
			search_index = index_init + i + j * mapData.info.width;
			cell_data = mapData.data[search_index];
			if(cell_data > free_th){
				return OCCUPIED;
			}
			else if(cell_data == -1){
				unexplored = true;
			}

			if(search_index % mapData.info.width == 0){ // If index is at limit of X, break so there is no next index error
				break;
			}
		}

		if(search_index + 1 >= mapData.data.size()){ // If index is at limit of Y, break so there is no next index error
			break;
		}
	}

	if(unexplored){
		return UNEXPLORED;
	}
	else{
		return FREE;
	}
}




void FRONTIERS_FILTER::rviz_show_filtered_frontiers(){
    visualization_msgs::Marker filtered_frontier_point_marker;
	geometry_msgs::Point position;

    filtered_frontier_point_marker.header.frame_id = "world";
    filtered_frontier_point_marker.header.stamp = ros::Time();
    filtered_frontier_point_marker.ns = "filtered_frontier points";
    filtered_frontier_point_marker.action = visualization_msgs::Marker::ADD;
    filtered_frontier_point_marker.id = 2;
    filtered_frontier_point_marker.lifetime = ros::Duration();
    filtered_frontier_point_marker.type = visualization_msgs::Marker::SPHERE_LIST;
    filtered_frontier_point_marker.scale.x = filtered_frontier_point_marker.scale.y = filtered_frontier_point_marker.scale.z = map_resolution;
    filtered_frontier_point_marker.color.a = 1.0;
    filtered_frontier_point_marker.color.r = 0.0;
    filtered_frontier_point_marker.color.g = 0.0;
    filtered_frontier_point_marker.color.b = 1.0;
    filtered_frontier_point_marker.pose.orientation.w = 1.0;

	for(auto ptr = filtered_frontier_points.begin() ; ptr != filtered_frontier_points.end() ; ++ptr){
		position.x = ptr -> x();
		position.y = ptr -> y();
		position.z = ptr -> z();
		filtered_frontier_point_marker.points.push_back(position);
	}

	rviz_filtered_frontier_points_pub.publish(filtered_frontier_point_marker);
}




void FRONTIERS_FILTER::clustering_filter(Eigen::Vector3d& point){
	if(filtered_frontier_points.size() == 0){
		filtered_frontier_points.push_back(point);
	}

	else{
		for(auto ptr = filtered_frontier_points.begin() ; ptr != filtered_frontier_points.end() ; ++ptr){
			if(get_point2point_distance(point, *ptr) < clustering_radius){
				return;
			}
		}
		filtered_frontier_points.push_back(point);
	}
}




bool FRONTIERS_FILTER::is_explored(const Eigen::Vector3d& point) const{
	return check_space_state(point, robot_radius + resolution) != UNEXPLORED;
}




void FRONTIERS_FILTER::update_frontier_points(vector<Eigen::Vector3d>& frontier_points_list){
	ofp_planner::point_array filtered_frontiers_msg;
	geometry_msgs::Point filtered_frontier;

	if(frontier_points_list.size() > 0){
		//ROS_INFO("[%zu]", frontier_points_list.size());
		auto it = remove_if(frontier_points_list.begin(), frontier_points_list.end(), bind(&FRONTIERS_FILTER::is_explored, this, placeholders::_1));
		frontier_points_list.erase(it, frontier_points_list.end());

		for(auto ptr = frontier_points_list.begin() ; ptr != frontier_points_list.end() ; ++ptr){
			filtered_frontier.x = ptr -> x(); filtered_frontier.y = ptr -> y(); filtered_frontier.z = ptr -> z();
			filtered_frontiers_msg.points.push_back(filtered_frontier);
		}
		filtered_frontier_points_pub.publish(filtered_frontiers_msg);
	}

	rviz_show_filtered_frontiers();
}




void FRONTIERS_FILTER::init(){
	/*ros::WallDuration wall_duration;
	ros::WallTime wall_begin, wall_now;*/

	while(ros::ok() && filtered_frontier_points.size() == 0){ros::spinOnce();} // Wait for first frontiers before init

	while(ros::ok()){
		//wall_begin = ros::WallTime::now();	
		ros::spinOnce();
		update_frontier_points(filtered_frontier_points);
		/*wall_now = ros::WallTime::now();
		wall_duration = wall_now - wall_begin;
		ROS_WARN("WALL FRONTIERS FILTER: %u.%09u s", wall_duration.sec, wall_duration.nsec);*/
	}
}