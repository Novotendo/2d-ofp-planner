#include "planning_lib.h"




double get_point2point_distance(Eigen::Vector3d q1, Eigen::Vector3d q2){
	double distance = sqrt(pow(q2.x() - q1.x(), 2.0) + pow(q2.y() - q1.y(), 2.0) + pow(q2.z() - q1.z(), 2.0));
	return distance;
}




vector<Eigen::Vector3d> increase_resolution(Eigen::Vector3d q0, Eigen::Vector3d q1, int interpolator){
	Eigen::Vector3d q_int;
	vector<Eigen::Vector3d> qn;
	//NOTE: interpolator = distance/resolution;
	if(interpolator > 1){ // If new nodes can be interpolated in between q0 and q1, lets create them
		for(int i = 1 ; i < interpolator ; i++){
			q_int.x() = q0.x() + ((q1.x()-q0.x())/interpolator)*i; 
			q_int.y() = q0.y() + ((q1.y()-q0.y())/interpolator)*i;
			q_int.z() = q0.z() + ((q1.z()-q0.z())/interpolator)*i;

			qn.push_back(q_int);
	   	}
	}

	qn.push_back(q1); // Store also the final node

	return qn;
}