#include "planning_lib.h"
#include "fast_rrt_star.h"


int main(int argc, char** argv){
    ros::init(argc, argv, "fast_rrt_planner_node");
    ros::NodeHandle nh_("");
    ros::NodeHandle nh_private_("~");

    FAST_RRT_STAR_PLANNER fast_rrt_star_planner(nh_, nh_private_);
    fast_rrt_star_planner.init();

    return 0;
}