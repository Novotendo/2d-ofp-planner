#include <exploration_manager.h>

EXPLORATION_MANAGER::EXPLORATION_MANAGER(const ros::NodeHandle& nh, const ros::NodeHandle& nh_private)
    : nh_(nh),
      nh_private_(nh_private){

    nh_private_.param("exploration_height", exploration_height, exploration_height);
    nh_private_.param("robot_radius", robot_radius, robot_radius);

    mav_pose_pub = nh_private_.advertise<geometry_msgs::PoseStamped>("/hummingbird/command/pose", 1, true);
    planner_request_pub = nh_private_.advertise<geometry_msgs::PointStamped>("/fast_rrt_star/request_plan", 1, true);

    mav_pose_sub = nh_private_.subscribe("/odom", 1000, &EXPLORATION_MANAGER::mav_pose_callback, this);
    init_exploration_sub = nh_private_.subscribe("/init_fancy_exploration", 1, &EXPLORATION_MANAGER::init_exploration_callback, this);
    finish_exploration_sub = nh_private_.subscribe("/finish_fancy_exploration", 1, &EXPLORATION_MANAGER::finish_exploration_callback, this);
    explore_point_sub = nh_private_.subscribe("/optimal_frontier_point", 1000, &EXPLORATION_MANAGER::explore_point_callback, this);

    mav_navigating = false;
    return_home = false;
    mav_pose_received = false;
    set_up = true;
    home_position << 0.0, 0.0, 0.0;
}




void EXPLORATION_MANAGER::init_exploration_callback(const std_msgs::Bool& msg){
    if(msg.data == true && set_up == true){
        mav_take_off(exploration_height);
        systemRet = system("roslaunch ofp_planner hector_slam.launch &"); 
        if(systemRet == -1){
            exit(0);
        }
        ros::Duration(3.0).sleep();

        set_up = false;
        
        /*systemRet = system("roslaunch ofp_planner novo_ofp_exploration.launch &");
        if(systemRet == -1){
            exit(0);
        }
        ros::Duration(3.0).sleep();*/
    }
}




void EXPLORATION_MANAGER::finish_exploration_callback(const std_msgs::Bool& msg){
    if(msg.data == true && return_home == false && mav_navigating == false){
        systemRet = system("rosnode kill ofp_assigner_node &"); 
        if(systemRet == -1){
            exit(0);
        }
        systemRet = system("rosnode kill frontiers_filter_node &"); 
        if(systemRet == -1){
            exit(0);
        }
        systemRet = system("rosnode kill global_frontier_detector_node &"); 
        if(systemRet == -1){
            exit(0);
        }

        ros::Duration(3.0).sleep();

        return_home = true;
        ROS_INFO("Exploration Completed! Returning Home!");
    }
}




void EXPLORATION_MANAGER::explore_point_callback(const geometry_msgs::Point::ConstPtr& msg){
    if(!mav_navigating){
        point2explore.point.x = msg -> x;
        point2explore.point.y = msg -> y;
        point2explore.point.z = exploration_height;//msg -> z;

        planner_request_pub.publish(point2explore);
        mav_navigating = true;
    }
}




void EXPLORATION_MANAGER::mav_pose_callback(const geometry_msgs::PoseStamped::ConstPtr& msg){
	double roll, pitch, yaw;
    mav_current_pose.x() = msg -> pose.position.x;
    mav_current_pose.y() = msg -> pose.position.y;
    mav_current_pose.z() = exploration_height;//msg -> pose.position.z;

    mav_current_quaternion.setX(msg -> pose.orientation.x);
    mav_current_quaternion.setY(msg -> pose.orientation.y);
    mav_current_quaternion.setZ(msg -> pose.orientation.z);
    mav_current_quaternion.setW(msg -> pose.orientation.w);

    tf::Matrix3x3 m(mav_current_quaternion);
    m.getRPY(roll, pitch, yaw);
	mav_current_pose.w() = yaw;
    mav_pose_received = true;
}




void EXPLORATION_MANAGER::mav_take_off(const double& height) const{
    geometry_msgs::PoseStamped take_off_pose;

    take_off_pose.pose.position.x = mav_current_pose.x();
    take_off_pose.pose.position.y = mav_current_pose.y();
    take_off_pose.pose.position.z = height;

    mav_pose_pub.publish(take_off_pose);

    ros::Duration(2.0).sleep();
}




void EXPLORATION_MANAGER::mav_land() const{
    geometry_msgs::PoseStamped land_pose;

    land_pose.pose.position.x = mav_current_pose.x();
    land_pose.pose.position.y = mav_current_pose.y();
    land_pose.pose.position.z = 0.0;

    mav_pose_pub.publish(land_pose);

    ros::Duration(2.0).sleep();
}




bool EXPLORATION_MANAGER::mav_goal_arrived() const{
	Eigen::Vector3d current_position, desired_position;

	current_position << mav_current_pose.x(), mav_current_pose.y(), mav_current_pose.z();
    desired_position << point2explore.point.x, point2explore.point.y, point2explore.point.z;
    if(get_point2point_distance(current_position, desired_position) < robot_radius){
        ros::Duration(2.0).sleep();
        return true;
    }
    else{
        return false;
    }
}




void EXPLORATION_MANAGER::mav_check_surroundings(const double& yaw) const{
    tf::Quaternion quaternion;
    geometry_msgs::PoseStamped pose;
    unsigned int steps = 100;
    double d_yaw;

    pose.pose.position.x = mav_current_pose.x();
    pose.pose.position.y = mav_current_pose.y();
    pose.pose.position.z = mav_current_pose.z();

    for(unsigned int j = 0; j < steps ; j++){
        d_yaw = j*yaw/steps;
        quaternion.setRPY(0, 0, d_yaw);
        pose.pose.orientation.x = quaternion.x();
        pose.pose.orientation.y = quaternion.y();
        pose.pose.orientation.z = quaternion.z();
        pose.pose.orientation.w = quaternion.w();
        mav_pose_pub.publish(pose);
        ros::Duration(0.1).sleep();
    }
}




void EXPLORATION_MANAGER::init(){

    while(ros::ok()){ // Wait for set up completed before init
        ros::spinOnce();
        if(set_up == false && mav_pose_received == true){
            mav_check_surroundings(2*M_PI);
            systemRet = system("roslaunch ofp_planner novo_ofp_exploration.launch &");
            if(systemRet == -1){
                exit(0);
            }
            ros::Duration(3.0).sleep();
            ROS_INFO("Set Up Completed! Initializing Exploration Process!");
            break;
        }
    }

    while(ros::ok()){
        ros::spinOnce();

        if(mav_navigating){
            if(mav_goal_arrived()){
                ros::Duration(2.0).sleep();
                if(return_home){
                    mav_land();
                    ROS_INFO("In Home...\n");
                    return_home = false;
                }
                mav_navigating = false;
            }
        }

        else if(return_home){
            point2explore.point.x = 0.0;
            point2explore.point.y = 0.0;
            point2explore.point.z = exploration_height;//msg -> z;

            planner_request_pub.publish(point2explore);
            mav_navigating = true;
        }
    }
}