#include "fast_rrt_star.h"
#include "planning_lib.h"

	
FAST_RRT_STAR_PLANNER::FAST_RRT_STAR_PLANNER(const ros::NodeHandle& nh, const ros::NodeHandle& nh_private)
    : nh_(nh),
      nh_private_(nh_private){

	nh_private_.param("robot_radius", robot_radius, robot_radius);
	nh_private_.param("resolution", resolution, resolution);
	nh_private_.param("neighbours_radius", neighbours_radius, neighbours_radius);
	nh_private_.param("exploration_height", exploration_height, exploration_height);

	rviz_init_and_goal_pub = nh_private_.advertise<visualization_msgs::MarkerArray>("/fast_rrt_star/rviz/init_goal", 1, true);
	rviz_tree_pub = nh_private_.advertise<visualization_msgs::MarkerArray>("/fast_rrt_star/rviz/tree", 1, true);
	rviz_optimal_path_pub = nh_private_.advertise<visualization_msgs::MarkerArray>("/fast_rrt_star/rviz/optimal_path", 1, true);
	mav_trajectory_pub = nh_private_.advertise<trajectory_msgs::MultiDOFJointTrajectory>("/hummingbird/command/trajectory", 1, true);
	mav_pose_pub = nh_private_.advertise<geometry_msgs::PoseStamped>("/hummingbird/command/pose", 1, true);

	map_sub = nh_private_.subscribe("/map", 10, &FAST_RRT_STAR_PLANNER::map_callback, this);
	mav_pose_sub = nh_private_.subscribe("/odom", 1000, &FAST_RRT_STAR_PLANNER::mav_pose_callback, this);
	rviz_clicket_point_sub = nh_private_.subscribe("/clicked_point", 100, &FAST_RRT_STAR_PLANNER::tree_init_callback, this);
	request_plan_sub = nh_private_.subscribe("/fast_rrt_star/request_plan", 100, &FAST_RRT_STAR_PLANNER::tree_init_callback, this);

	GOAL_FOUND = false;
	plan_request = false;
	planner_working = false;
}




void FAST_RRT_STAR_PLANNER::generate_random_point(Eigen::Vector3d& random_point) const{
	random_point.x() = (double) (map_lower_bound.x() + (rand() / (RAND_MAX + 1.0)) * (map_upper_bound.x() - map_lower_bound.x())); 
	random_point.y() = (double) (map_lower_bound.y() + (rand() / (RAND_MAX + 1.0)) * (map_upper_bound.y() - map_lower_bound.y())); 
	random_point.z() = exploration_height;//mav_current_pose.z();
}




double FAST_RRT_STAR_PLANNER::get_node2node_distance(NODE* q1, NODE* q2){
	return get_point2point_distance(q1 -> get_node_xyz(), q2 -> get_node_xyz());
}




void FAST_RRT_STAR_PLANNER::load_optimal_path2seed(NODE* node){ // Function to print new edge when publishing the final topic
    geometry_msgs::Point xyz_1, xyz_2;
    visualization_msgs::Marker edge;
	NODE* node_path_searcher = node;
    edge.header.frame_id = "world";
    edge.ns = "fast_rrt_star_planner_optimal_path";
    edge.action = visualization_msgs::Marker::ADD;
    edge.type = visualization_msgs::Marker::ARROW;
    edge.scale.x = 0.05;
	edge.scale.y = 0.00001;
	edge.scale.z = 0.00001;
    edge.color.a = 1.0;
    edge. color.r = 1.0;
    edge.color.g = 1.0;
    edge.color.b = 0.0;
    edge.pose.orientation.w = 1.0;

	this -> OPTIMAL_PATH.push_back(node_path_searcher);

	do{
		this -> OPTIMAL_PATH.push_back(node_path_searcher -> parent);

		edge.header.stamp = ros::Time();
		edge.id = OPTIMAL_PATH.size();
		edge.lifetime = ros::Duration();

		xyz_1.x = node_path_searcher -> parent -> get_node_xyz().x();
		xyz_1.y = node_path_searcher -> parent -> get_node_xyz().y();
		xyz_1.z = node_path_searcher -> parent -> get_node_xyz().z();

		xyz_2.x = node_path_searcher -> get_node_xyz().x();
		xyz_2.y = node_path_searcher -> get_node_xyz().y();
		xyz_2.z = node_path_searcher -> get_node_xyz().z();

		edge.points.push_back(xyz_1); // Saving point 1
		edge.points.push_back(xyz_2); // Saving point 2 because a edge needs to store both conected points consecutive
		this -> tree_optimal_path.markers.push_back(edge);
		rviz_optimal_path_pub.publish(this -> tree_optimal_path);
		edge.points.clear(); 

		node_path_searcher = node_path_searcher -> parent;
	}while(node_path_searcher -> get_node_index() != 0);

	reverse(this -> OPTIMAL_PATH.begin(), this -> OPTIMAL_PATH.end()); // Reverse so sequence starts at SEED point to create trajectory to goal point*/
}




double FAST_RRT_STAR_PLANNER::find_nearest_node(Eigen::Vector3d& q, NODE& q_nearest){
	double distance = 0.0;
	double min_distance = numeric_limits<double>::infinity(); // Set initial minimum distance to compare as inf

	for(auto ptr = TREE.begin() ; ptr != TREE.end() ; ++ptr){ // For every single node in tree
		distance = get_point2point_distance(q, (*ptr) -> get_node_xyz()); // Get distance to new possible node

		if(distance < min_distance){ // Compare if new distance is less than the current minimum one
			min_distance = distance; // In case a nearest node in tree is found
			q_nearest = **ptr; // Store it
		}
	}
	return min_distance;
}




bool FAST_RRT_STAR_PLANNER::check_if_goal_found(NODE* q){
	if(get_point2point_distance(q -> get_node_xyz(), GOAL) < robot_radius){//error){ // If distance to goal is close enough, then goal is considered arrived
		return true;
	}

	else{
		return false;
	}	
}




vector<NODE*> FAST_RRT_STAR_PLANNER::find_neighbours(NODE* node){
	vector<NODE*> neighbours;
	double distance2neighbour;
	for(auto ptr = TREE.begin() ; ptr != TREE.end() ; ++ptr){ // For every single node in tree
		distance2neighbour = get_point2point_distance(node -> get_node_xyz(), (*ptr) -> get_node_xyz());
		if((distance2neighbour <= neighbours_radius) & (distance2neighbour > 0)){
			neighbours.push_back(*ptr);
		}
	}	
	return neighbours;
}




bool comp(NODE* const & a, NODE* const & b){
  return a -> data.distance_travelled < b -> data.distance_travelled;
}




void FAST_RRT_STAR_PLANNER::reconnect(NODE* node){
	vector<NODE*> neighbours;
	NODE* best_parent;
	vector<Eigen::Vector3d> interpolated_nodes;
	double distance2parent;
	bool new_extension;
	bool reinit;
	
	if(node -> get_node_index() != 0){
		neighbours = find_neighbours(node);
		if(neighbours.size() > 0){
			sort(begin(neighbours), end(neighbours), comp);
			for(auto ptr = neighbours.begin() ; ptr != neighbours.end() ; ++ptr){
				reinit = false;
				best_parent = *ptr;
				distance2parent = get_node2node_distance(best_parent, node);
				interpolated_nodes = increase_resolution(best_parent -> get_node_xyz(), node -> get_node_xyz(), (int)(distance2parent/resolution));
				interpolated_nodes.pop_back();

				for(auto ptr = interpolated_nodes.begin() ; ptr != interpolated_nodes.end() ; ++ptr){
					if(!check_if_free_space(*ptr)){
						reinit = true;
						break;
					}
				}

				if(reinit == false){
					if(interpolated_nodes.size() > 0){
						new_extension = 1;
						for(auto ptr = interpolated_nodes.begin() ; ptr != interpolated_nodes.end() ; ++ptr){
							TREE.push_back(new NODE(TREE.size(), *ptr, &tree_edges));
							if(new_extension == 1){ // If new root is going to be added
								TREE.at(best_parent -> get_node_index()) -> adopt_child(TREE.back()); // Create that root on tree
								new_extension = 0;
							}
							else{ // If new root has been added to tree, then it can grow
								TREE.rbegin()[1] -> adopt_child(TREE.back());
							}
							TREE.back() -> data.distance_travelled = TREE.back() -> parent -> data.distance_travelled + get_node2node_distance(TREE.back(), TREE.back() -> parent);

							rviz_tree_pub.publish(tree_edges);
						}

						TREE.back() -> adopt_child(TREE.at(node -> get_node_index()));
						rviz_tree_pub.publish(tree_edges);
					}

					else{
						best_parent -> adopt_child(node);
					}

					break;
				}
			}			
		}	

		TREE.at(node -> get_node_index()) -> data.distance_travelled = node -> parent -> data.distance_travelled + get_node2node_distance(node, node -> parent);
		rviz_tree_pub.publish(tree_edges);

		reconnect(node -> parent);
	}

	else{
		ROS_INFO("Optimal path found!");
		return;
	}
}




bool FAST_RRT_STAR_PLANNER::mav_goal_arrived() const{
	Eigen::Vector3d current_position;
	current_position << mav_current_pose.x(), mav_current_pose.y(), mav_current_pose.z();
    if(get_point2point_distance(current_position, GOAL) <= robot_radius){
        ROS_INFO("MAV reached the goal succesfully!");
        return true;
    }
    else{
        return false;
    }
}




void FAST_RRT_STAR_PLANNER::delete_tree(){
	if(TREE.size() > 0){
		visualization_msgs::Marker point;
		for (size_t i = 0; i < TREE.size(); ++i){
			delete TREE[i];
		}

		TREE.clear(); // Deleting planner data
		OPTIMAL_PATH.clear();

		point.header.frame_id = "world";
		point.action = visualization_msgs::Marker::DELETEALL;

		point.ns = "fast_rrt_star_planner_edges";
		point.header.stamp = ros::Time();
		tree_edges.markers.push_back(point); 

		rviz_tree_pub.publish(tree_edges);

		point.ns = "fast_rrt_star_planner_optimal_path";
		point.header.stamp = ros::Time();
		tree_optimal_path.markers.push_back(point); 

		rviz_optimal_path_pub.publish(tree_optimal_path);

		point.ns = "init_goal";
		point.header.stamp = ros::Time();
		init_goal.markers.push_back(point); 

		rviz_init_and_goal_pub.publish(init_goal);

		tree_edges.markers.clear();
		rviz_tree_pub.publish(tree_edges);

		tree_optimal_path.markers.clear();
		rviz_optimal_path_pub.publish(tree_optimal_path);

		init_goal.markers.clear();
		rviz_init_and_goal_pub.publish(init_goal);

		if(TREE.size() == 0 && OPTIMAL_PATH.size() == 0){
			ROS_INFO("Planner data deleted!\n");
		}

		else{
			ROS_INFO("[ERROR]: Planner data could not be deleted!");
		}
	}    
}




void FAST_RRT_STAR_PLANNER::mav_pose_callback(const geometry_msgs::PoseStamped::ConstPtr& msg){
	double roll, pitch, yaw;
    mav_current_pose.x() = msg -> pose.position.x;
    mav_current_pose.y() = msg -> pose.position.y;
    mav_current_pose.z() = exploration_height;//msg -> pose.position.z;

    mav_current_quaternion.setX(msg -> pose.orientation.x);
    mav_current_quaternion.setY(msg -> pose.orientation.y);
    mav_current_quaternion.setZ(msg -> pose.orientation.z);
    mav_current_quaternion.setW(msg -> pose.orientation.w);

    tf::Matrix3x3 m(mav_current_quaternion);
    m.getRPY(roll, pitch, yaw);
	mav_current_pose.w() = yaw;
}




void FAST_RRT_STAR_PLANNER::tree_init_callback(const geometry_msgs::PointStamped& msg){
	reset();
	ROS_INFO("New plan request!");

	GOAL.x() = msg.point.x;
	GOAL.y() = msg.point.y;
	GOAL.z() = msg.point.z;

	ROS_INFO("GOAL: [%f] [%f] [%f]", GOAL.x(), GOAL.y(), GOAL.z());

	compute_map_bounds(map_lower_bound, map_upper_bound);

	visualization_msgs::Marker point_2d;
	point_2d.header.frame_id = "world";
	point_2d.header.stamp = ros::Time();
	point_2d.ns = "init_goal";
	point_2d.action = visualization_msgs::Marker::ADD;
	point_2d.id = 1;
	point_2d.lifetime = ros::Duration();
	point_2d.type = visualization_msgs::Marker::CYLINDER;
	point_2d.scale.x = point_2d.scale.y = 2 * robot_radius;
	point_2d.scale.z = 0.001;
	point_2d.color.a = 1.0;
	point_2d.pose.orientation.w = 1.0;

	point_2d.pose.position.x = GOAL.x();
	point_2d.pose.position.y = GOAL.y();
	point_2d.pose.position.z = GOAL.z();


	if(check_if_free_space(GOAL)){
		point_2d.color.r = 0.0;
		point_2d.color.g = 1.0;
		point_2d.color.b = 0.0;
		init_goal.markers.push_back(point_2d);
		rviz_init_and_goal_pub.publish(init_goal);
		plan_request = true;
	}
	else{
		point_2d.color.r = 1.0;
		point_2d.color.g = 0.0;
		point_2d.color.b = 0.0;
		init_goal.markers.push_back(point_2d);
		rviz_init_and_goal_pub.publish(init_goal);
		ROS_INFO("GOAL is not reachable!");
	}
}




void FAST_RRT_STAR_PLANNER::map_callback(const nav_msgs::OccupancyGrid::ConstPtr& msg){
    mapData = *msg;
}




unsigned int FAST_RRT_STAR_PLANNER::get_index_of_point(const Eigen::Vector3d& point) const{
	Eigen::Vector2d fixed_map_point;
	fixed_map_point.x() = point.x() - map_x_init;
	fixed_map_point.y() = point.y() - map_y_init;
	return (int)(floor(fixed_map_point.y() / map_resolution) * mapData.info.width + floor(fixed_map_point.x() / map_resolution));
}




void FAST_RRT_STAR_PLANNER::compute_map_bounds(Eigen::Vector2d& lower_bound, Eigen::Vector2d& upper_bound){
	map_width = mapData.info.width * mapData.info.resolution;
	map_height = mapData.info.height * mapData.info.resolution;
	map_x_init = mapData.info.origin.position.x;
	map_y_init = mapData.info.origin.position.y;
	map_resolution = mapData.info.resolution;

	lower_bound.x() = map_x_init;
	lower_bound.y() = map_y_init;

	upper_bound.x() = map_x_init + map_width;
	upper_bound.y() = map_y_init + map_height;
}




bool FAST_RRT_STAR_PLANNER::check_if_free_space(Eigen::Vector3d position){
	unsigned int index_center, fixed_radius, index_init, search_index;
	bool unexplored;
	int8_t cell_data;
	index_center = get_index_of_point(position);
	fixed_radius = (int)(robot_radius / map_resolution);
	index_init = index_center - fixed_radius * (mapData.info.width + 1);

	unexplored = false;

	for(unsigned int i = 0 ; i < (2 * fixed_radius + 1) ; i++){
		for(unsigned int j = 0 ; j < (2 * fixed_radius + 1) ; j++){
			search_index = index_init + i + j * mapData.info.width;
			cell_data = mapData.data[search_index];
			if(cell_data > free_th){
				return false;
			}
			else if(cell_data == -1){
				unexplored = true;
			}

			if(search_index % mapData.info.width == 0){ // If index is at limit of X, break so there is no next index error
				break;
			}
		}

		if(search_index + 1 >= mapData.data.size()){ // If index is at limit of Y, break so there is no next index error
			break;
		}
	}

	if(unexplored){
		return false;
	}
	else{
		return true;
	}
}




space_state FAST_RRT_STAR_PLANNER::check_space_state(const Eigen::Vector3d& position) const{
	unsigned int index_center, fixed_radius, index_init, search_index;
	bool unexplored;
	int8_t cell_data;
	index_center = get_index_of_point(position);
	fixed_radius = (int)(robot_radius / map_resolution);
	index_init = index_center - fixed_radius * (mapData.info.width + 1);

	unexplored = false;

	for(unsigned int i = 0 ; i < (2 * fixed_radius + 1) ; i++){
		for(unsigned int j = 0 ; j < (2 * fixed_radius + 1) ; j++){
			search_index = index_init + i + j * mapData.info.width;
			cell_data = mapData.data[search_index];
			if(cell_data > free_th){
				return OCCUPIED;
			}
			else if(cell_data == -1){
				unexplored = true;
			}

			if(search_index % mapData.info.width == 0){ // If index is at limit of X, break so there is no next index error
				break;
			}
		}

		if(search_index + 1 >= mapData.data.size()){ // If index is at limit of Y, break so there is no next index error
			break;
		}
	}

	if(unexplored){
		return UNEXPLORED;
	}
	else{
		return FREE;
	}
}




void FAST_RRT_STAR_PLANNER::mav_track_path(const vector<NODE*>& path2track){
    trajectory_msgs::MultiDOFJointTrajectory trajectory;
    double path_length, theta;

	MAV_PATH2TRACK.clear();
    path_length = 0.0;
   
    for(auto ptr = path2track.begin() ; ptr != path2track.end() ; ++ptr){
        MAV_PATH2TRACK.push_back((*ptr) -> get_node_xyz());
        if(ptr != path2track.end()-1){
            path_length = path_length + get_point2point_distance((*ptr) -> get_node_xyz(), (*(ptr+1)) -> get_node_xyz());
        }
    }

    ROS_INFO("Total distance: [%f]m", path_length);

    theta = mav_current_pose.w() + 2*M_PI * path_length / 2;
    build_3d_trajectory(trajectory, mav_current_pose.w(), theta);
    mav_trajectory_pub.publish(trajectory);

    ROS_INFO("Tracking requested trajectory!");
}




void FAST_RRT_STAR_PLANNER::vertex4trajectory(mav_trajectory_generation::Vertex::Vector& vertices, vector<Eigen::Vector3d>& path, double& init_yaw, double& final_yaw){
    mav_trajectory_generation::Vertex start(dimension), middle(dimension), end(dimension);
    double delta_yaw = (final_yaw - init_yaw)/path.size();
    int j = 0;

    for(auto p = path.begin() ; p != path.end() ; p++){
        j = j+1;

        if(p == path.begin()){
            start.makeStartOrEnd(Eigen::Vector4d(p->x(), p->y(), p->z(), init_yaw), derivative_to_optimize);
            vertices.push_back(start);
        }

        else if(p == path.end()-1){
            end.makeStartOrEnd(Eigen::Vector4d(p->x(), p->y(), p->z(), final_yaw), derivative_to_optimize);
            vertices.push_back(end);
        }

        else{
            middle.addConstraint(mav_trajectory_generation::derivative_order::POSITION, Eigen::Vector4d(p->x(), p->y(), p->z(), (init_yaw + j*delta_yaw)));
            vertices.push_back(middle);
        }        
    }
}




void FAST_RRT_STAR_PLANNER::solve_linear_trajectory(mav_trajectory_generation::Vertex::Vector& vertices, mav_trajectory_generation::Trajectory& trajectory, double v_max, double a_max){
    std::vector<double> segment_times;
    segment_times = estimateSegmentTimes(vertices, v_max, a_max);
    const int N = 10;
    mav_trajectory_generation::PolynomialOptimization<N> opt(dimension);
    opt.setupFromVertices(vertices, segment_times, derivative_to_optimize);
    opt.solveLinear();
    opt.getTrajectory(&trajectory);
}




void FAST_RRT_STAR_PLANNER::sample_trajectory(mav_trajectory_generation::Trajectory& trajectory, trajectory_msgs::MultiDOFJointTrajectory& sampled_trajectory){
    mav_msgs::EigenTrajectoryPoint state;
    mav_msgs::EigenTrajectoryPoint::Vector states;
    double sampling_interval = 0.01;
    double delta_t = 0.0025;
    bool success = mav_trajectory_generation::sampleWholeTrajectory(trajectory, sampling_interval, &states);

    if(success){
        for(unsigned int j = 0; j < states.size() ; j++){
            trajectory_msgs::MultiDOFJointTrajectoryPoint my_point;
            geometry_msgs::Transform position_vector;

            position_vector.translation.x = states[j].position_W[0];
            position_vector.translation.y = states[j].position_W[1];
            position_vector.translation.z = states[j].position_W[2];

            position_vector.rotation.x = states[j].orientation_W_B.x();
            position_vector.rotation.y = states[j].orientation_W_B.y();
            position_vector.rotation.z = states[j].orientation_W_B.z();
            position_vector.rotation.w = states[j].orientation_W_B.w();

            my_point.transforms.push_back(position_vector);

            geometry_msgs::Twist vel_vector;

            vel_vector.linear.x = states[j].velocity_W[0];
            vel_vector.linear.y = states[j].velocity_W[1];
            vel_vector.linear.z = states[j].velocity_W[2];
            vel_vector.angular.x = states[j].angular_velocity_W[0];
            vel_vector.angular.y = states[j].angular_velocity_W[1];
            vel_vector.angular.z = states[j].angular_velocity_W[2];

            my_point.velocities.push_back(vel_vector);

            geometry_msgs::Twist acc_vector;

            acc_vector.linear.x = states[j].acceleration_W[0];
            acc_vector.linear.y = states[j].acceleration_W[1];
            acc_vector.linear.z = states[j].acceleration_W[2];
            acc_vector.angular.x = states[j].angular_acceleration_W[0];
            acc_vector.angular.y = states[j].angular_acceleration_W[1];
            acc_vector.angular.z = states[j].angular_acceleration_W[2];

            my_point.accelerations.push_back(acc_vector);

            sampled_trajectory.points.push_back(my_point);
            sampled_trajectory.header.stamp = ros::Time::now();
            sampled_trajectory.points[j].time_from_start = ros::Duration(j*delta_t);
        }
    }

    else{
        ROS_INFO("[ERROR]: Trajectory Generated Unsuccessfully!");
    }    
}




void FAST_RRT_STAR_PLANNER::build_3d_trajectory(trajectory_msgs::MultiDOFJointTrajectory& sampled_trajectory, double& init_yaw, double& final_yaw){
    vector<geometry_msgs::Point> path;
    mav_trajectory_generation::Vertex::Vector vertices;
    mav_trajectory_generation::Trajectory trajectory;

    vertex4trajectory(vertices, MAV_PATH2TRACK, init_yaw, final_yaw);
    solve_linear_trajectory(vertices, trajectory, vel_max, accel_max);
    sample_trajectory(trajectory, sampled_trajectory);
}




void FAST_RRT_STAR_PLANNER::expand_tree(Eigen::Vector3d& x_rand, NODE& x_near){
	bool new_root;
	int interpolator;
	Eigen::Vector3d q0, q1, q_int;
	double distance;
	q0 = x_near.get_node_xyz();
	q1 = x_rand;

	distance = get_point2point_distance(q0, q1);

	if(distance >= resolution){
		new_root = 1;
		interpolator = (int)(distance/resolution);

		for(int i = 1 ; i < interpolator ; i++){
			q_int.x() = q0.x() + ((q1.x()-q0.x())/interpolator)*i; 
			q_int.y() = q0.y() + ((q1.y()-q0.y())/interpolator)*i;
			q_int.z() = q0.z() + ((q1.z()-q0.z())/interpolator)*i;

			switch(check_space_state(q_int)){
				case FREE:
					TREE.push_back(new NODE(TREE.size(), q_int, &tree_edges));
					if(new_root == 1){ // If new root is going to be added
						TREE.at(x_near.get_node_index()) -> adopt_child(TREE.back()); // Create that root on tree
						new_root = 0;
					}
					else{ // If new root has been added to tree, then it can grow
						TREE.rbegin()[1] -> adopt_child(TREE.back());
					}
					TREE.back() -> data.distance_travelled = TREE.back() -> parent -> data.distance_travelled + get_node2node_distance(TREE.back(), TREE.back() -> parent);
					rviz_tree_pub.publish(tree_edges);	
					if(check_if_goal_found(TREE.back())){
						GOAL_FOUND = true;
						TREE.push_back(new NODE(TREE.size(), GOAL, &tree_edges));
						goal_node = TREE.back();
						return;
					}
					break;

				case OCCUPIED:
					return;

				case UNEXPLORED:
					return;
				
				default:
					break;
			}
		}
	}	
}




void FAST_RRT_STAR_PLANNER::init(){
	while(ros::ok()){
		ros::spinOnce();
		if(plan_request){
			if(!planner_working){
				mav_hover();
				SEED.x() = mav_current_pose.x();
				SEED.y() = mav_current_pose.y();
				SEED.z() = mav_current_pose.z();
				TREE.push_back(new NODE(TREE.size(), SEED, &tree_edges));
				ROS_INFO("SEED: [%f] [%f] [%f]", SEED.x(), SEED.y(), SEED.z());
				planner_working = true;
			}

			if(!GOAL_FOUND){
				compute_map_bounds(map_lower_bound, map_upper_bound);
				generate_random_point(random_position);
				find_nearest_node(random_position, q_nearest);
				expand_tree(random_position, q_nearest);
				if(GOAL_FOUND){
					reconnect(goal_node);
					load_optimal_path2seed(goal_node);
					mav_track_path(OPTIMAL_PATH);				
				}
			}

			if(mav_goal_arrived()){
				reset();
			}	
		} 
	}
}




void FAST_RRT_STAR_PLANNER::reset(){
	planner_working = false;
	plan_request = false;
	GOAL_FOUND = false;
	delete_tree();
	srand(time(NULL));
}




void FAST_RRT_STAR_PLANNER::mav_hover(){
	geometry_msgs::PoseStamped hovering_pose;

	hovering_pose.pose.orientation.x = mav_current_quaternion.x();
	hovering_pose.pose.orientation.y = mav_current_quaternion.y();
	hovering_pose.pose.orientation.z = mav_current_quaternion.z();
	hovering_pose.pose.orientation.w = mav_current_quaternion.w();

	hovering_pose.pose.position.x = mav_current_pose.x();
	hovering_pose.pose.position.y = mav_current_pose.y();
	hovering_pose.pose.position.z = mav_current_pose.z();

	mav_pose_pub.publish(hovering_pose);
}