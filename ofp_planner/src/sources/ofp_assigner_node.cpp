#include <ofp_assigner.h>

int main(int argc, char** argv){
    ros::init(argc, argv, "ofp_assigner_node");
    ros::NodeHandle nh_("");
    ros::NodeHandle nh_private_("~");
    ros::Rate loop_rate(40);


    OFP_ASSIGNER ofp_assigner(nh_, nh_private_);
    ROS_INFO("Initialized OFP Assigner node.");
    ofp_assigner.init();

    /*while(ros::ok()){
        ros::spinOnce();

        loop_rate.sleep();
    }*/

    //ros::spin();

    return 0;
}