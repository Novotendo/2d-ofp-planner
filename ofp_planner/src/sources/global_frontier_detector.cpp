#include <global_frontier_detector.h>

GLOBAL_FRONTIER_DETECTOR::GLOBAL_FRONTIER_DETECTOR(const ros::NodeHandle& nh, const ros::NodeHandle& nh_private)
    : nh_(nh),
      nh_private_(nh_private){

    nh_private_.param("robot_radius", robot_radius, robot_radius);
	nh_private_.param("resolution", resolution, resolution);
	nh_private_.param("free_th", free_th, free_th);

	map_sub = nh_private_.subscribe("/map", 10, &GLOBAL_FRONTIER_DETECTOR::map_callback, this);

	rviz_tree_pub = nh_private_.advertise<visualization_msgs::MarkerArray>("/rviz/global_frontier_detector_tree", 1, true);
	rviz_frontier_points_pub = nh_private_.advertise<visualization_msgs::Marker>("/rviz/global_frontier_points", 1);
	frontier_points_pub = nh_private_.advertise<geometry_msgs::Point>("/global_frontier_points", 1);
}




void GLOBAL_FRONTIER_DETECTOR::map_callback(const nav_msgs::OccupancyGrid::ConstPtr& msg){
    mapData = *msg;
}




void GLOBAL_FRONTIER_DETECTOR::compute_map_bounds(Eigen::Vector2d& lower_bound, Eigen::Vector2d& upper_bound){
	ros::spinOnce();

	map_width = mapData.info.width * mapData.info.resolution;
	map_height = mapData.info.height * mapData.info.resolution;
	map_x_init = mapData.info.origin.position.x;
	map_y_init = mapData.info.origin.position.y;
	map_resolution = mapData.info.resolution;

	lower_bound.x() = map_x_init;
	lower_bound.y() = map_y_init;

	upper_bound.x() = map_x_init + map_width;
	upper_bound.y() = map_y_init + map_height;

	/*ROS_INFO("Map lower bounds: X = [%f], Y = [%f]", lower_bound.x(), lower_bound.y());
	ROS_INFO("Map upper bounds: X = [%f], Y = [%f] \n", upper_bound.x(), upper_bound.y());*/
}




unsigned int GLOBAL_FRONTIER_DETECTOR::get_index_of_point(const Eigen::Vector3d& point) const{
	Eigen::Vector2d fixed_map_point;
	fixed_map_point.x() = point.x() - map_x_init;
	fixed_map_point.y() = point.y() - map_y_init;
	return (int)(floor(fixed_map_point.y() / map_resolution) * mapData.info.width + floor(fixed_map_point.x() / map_resolution));
}




Eigen::Vector3d GLOBAL_FRONTIER_DETECTOR::get_point_of_index(const unsigned int& index) const{
	Eigen::Vector3d point;
	point.x() = map_resolution * (index - mapData.info.width * floor(index / mapData.info.width) + 0.5) + map_x_init;
	point.y() = map_resolution * (floor(index / mapData.info.width) + 0.5) + map_y_init;
	point.z() = 0.0;
	return point;
}




space_state GLOBAL_FRONTIER_DETECTOR::check_space_state(const Eigen::Vector3d& position, const double& radius) const{
	unsigned int index_center, fixed_radius, index_init, search_index;
	bool unexplored;
	int8_t cell_data;
	index_center = get_index_of_point(position);
	fixed_radius = (int)(radius / map_resolution);
	index_init = index_center - fixed_radius * (mapData.info.width + 1);

	unexplored = false;

	for(unsigned int i = 0 ; i < (2 * fixed_radius + 1) ; i++){
		for(unsigned int j = 0 ; j < (2 * fixed_radius + 1) ; j++){
			search_index = index_init + i + j * mapData.info.width;
			cell_data = mapData.data[search_index];
			if(cell_data > free_th){
				return OCCUPIED;
			}
			else if(cell_data == -1){
				unexplored = true;
			}

			if(search_index % mapData.info.width == 0){ // If index is at limit of X, break so there is no next index error
				break;
			}
		}

		if(search_index + 1 >= mapData.data.size()){ // If index is at limit of Y, break so there is no next index error
			break;
		}
	}

	if(unexplored){
		return UNEXPLORED;
	}
	else{
		return FREE;
	}
}




void GLOBAL_FRONTIER_DETECTOR::generate_random_point(Eigen::Vector3d& random_point) const{
	random_point.x() = (double) (map_lower_bound.x() + (rand() / (RAND_MAX + 1.0)) * (map_upper_bound.x() - map_lower_bound.x())); 
	random_point.y() = (double) (map_lower_bound.y() + (rand() / (RAND_MAX + 1.0)) * (map_upper_bound.y() - map_lower_bound.y())); 
	random_point.z() = 0.0;
}




double GLOBAL_FRONTIER_DETECTOR::get_node2node_distance(NODE& q1, NODE& q2){
	return get_point2point_distance(q1.get_node_xyz(), q2.get_node_xyz());
}




double GLOBAL_FRONTIER_DETECTOR::find_nearest_node(Eigen::Vector3d& q, NODE& q_nearest){
	double distance = 0.0;
	double min_distance = numeric_limits<double>::infinity(); // Set initial minimum distance to compare as inf

	for(auto ptr = TREE.begin() ; ptr != TREE.end() ; ++ptr){ // For every single node in tree
		distance = get_point2point_distance(q, (*ptr) -> get_node_xyz()); // Get distance to new possible node

		if(distance < min_distance){ // Compare if new distance is less than the current minimum one
			min_distance = distance; // In case a nearest node in tree is found
			q_nearest = **ptr; // Store it
		}
	}
	return min_distance;
}




void GLOBAL_FRONTIER_DETECTOR::expand_tree(Eigen::Vector3d& x_rand, NODE& x_near){
	bool new_root, new_frontier, new_obstacle;
	int interpolator;
	Eigen::Vector3d q0, q1, q_int, q_last;
	double distance;
	geometry_msgs::Point frontier_point;
	q0 = x_near.get_node_xyz();
	q1 = x_rand;

	distance = get_point2point_distance(q0, q1);

	if(distance >= resolution){
		new_root = 1;
		new_frontier = new_obstacle = 0;
		interpolator = (int)(distance/resolution);
		q_last = q0;

		for(int i = 1 ; i < interpolator ; i++){
			q_int.x() = q0.x() + ((q1.x()-q0.x())/interpolator)*i; 
			q_int.y() = q0.y() + ((q1.y()-q0.y())/interpolator)*i;
			q_int.z() = q0.z() + ((q1.z()-q0.z())/interpolator)*i;

			switch(check_space_state(q_int, robot_radius)){
				case FREE:
					TREE.push_back(new NODE(TREE.size(), q_int, &tree_edges));
					if(new_root == 1){ // If new root is going to be added
						TREE.at(x_near.get_node_index()) -> adopt_child(TREE.back()); // Create that root on tree
						new_root = 0;
					}
					else{ // If new root has been added to tree, then it can grow
						TREE.rbegin()[1] -> adopt_child(TREE.back());
					}
					rviz_tree_pub.publish(tree_edges);	
					break;

				case OCCUPIED:
					new_obstacle = 1;
					break;

				case UNEXPLORED:
					new_frontier = 1;
					q_int = q_last;
					frontier_points.push_back(q_int);
					frontier_point.x = q_int.x(); frontier_point.y = q_int.y(); frontier_point.z = q_int.z();
					frontier_points_pub.publish(frontier_point);
					break;
				
				default:
					break;
			}

			if(new_frontier || new_obstacle){
				break;
			}

			q_last = q_int;
		}
	}	
}




void GLOBAL_FRONTIER_DETECTOR::rviz_show_frontiers(){
    visualization_msgs::Marker frontier_point_marker, filtered_frontier_point_marker;
	geometry_msgs::Point position;

    frontier_point_marker.header.frame_id = "world";
    frontier_point_marker.header.stamp = ros::Time();
    frontier_point_marker.ns = "frontier_points";
    frontier_point_marker.action = visualization_msgs::Marker::ADD;
    frontier_point_marker.id = 1;
    frontier_point_marker.lifetime = ros::Duration();
    frontier_point_marker.type = visualization_msgs::Marker::SPHERE_LIST;
    frontier_point_marker.scale.x = frontier_point_marker.scale.y = frontier_point_marker.scale.z = 0.1;
    frontier_point_marker.color.a = 1.0;
    frontier_point_marker.color.r = 1.0;
    frontier_point_marker.color.g = 0.0;
    frontier_point_marker.color.b = 0.0;
    frontier_point_marker.pose.orientation.w = 1.0;

	for(auto ptr = frontier_points.begin() ; ptr != frontier_points.end() ; ++ptr){
		position.x = ptr -> x();
		position.y = ptr -> y();
		position.z = ptr -> z();
		frontier_point_marker.points.push_back(position);
	}

	rviz_frontier_points_pub.publish(frontier_point_marker);
}




bool GLOBAL_FRONTIER_DETECTOR::is_explored(const Eigen::Vector3d& point) const{
	return check_space_state(point, robot_radius + resolution) != UNEXPLORED;
}




void GLOBAL_FRONTIER_DETECTOR::update_frontier_points(vector<Eigen::Vector3d>& frontier_points_list){
	ros::spinOnce();

	if(frontier_points_list.size() > 1000){
		frontier_points_list.erase(frontier_points_list.begin(), frontier_points_list.begin() + 10);
	}

	auto it = remove_if(frontier_points_list.begin(), frontier_points_list.end(), bind(&GLOBAL_FRONTIER_DETECTOR::is_explored, this, placeholders::_1));
	frontier_points_list.erase(it, frontier_points_list.end());
}




void GLOBAL_FRONTIER_DETECTOR::init(){
	/*ros::WallDuration wall_duration;
	ros::WallTime wall_begin, wall_now;*/
	//ros::Rate loop_rate(40);

	srand(time(NULL)); // Randomize rand() seed sequence
	SEED << 0.0, 0.0, 0.0;
	TREE.push_back(new NODE(TREE.size(), SEED, &tree_edges));

	while(ros::ok()){
		//wall_begin = ros::WallTime::now();	
		compute_map_bounds(map_lower_bound, map_upper_bound);
		generate_random_point(random_position);
		find_nearest_node(random_position, q_nearest);
		expand_tree(random_position, q_nearest);
		update_frontier_points(frontier_points);
		rviz_show_frontiers();
		/*wall_now = ros::WallTime::now();
		wall_duration = wall_now - wall_begin;
		ROS_WARN("WALL GLOBAL FRONTIER DETECTOR: %u.%09u s", wall_duration.sec, wall_duration.nsec);*/
		//loop_rate.sleep();
	}
}