#include <global_frontier_detector.h>

int main(int argc, char** argv){
    ros::init(argc, argv, "global_frontier_detector_node");
    ros::NodeHandle nh_("");
    ros::NodeHandle nh_private_("~");
    ros::Rate loop_rate(40);


    GLOBAL_FRONTIER_DETECTOR global_frontier_detector(nh_, nh_private_);
    ROS_INFO("Initialized Global Frontiers Detector node.");
    global_frontier_detector.init();

    /*while(ros::ok()){
        ros::spinOnce();

        loop_rate.sleep();
    }*/

    //ros::spin();

    return 0;
}