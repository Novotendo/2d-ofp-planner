#include "planning_lib.h"
#include <exploration_manager.h>


int main(int argc, char** argv){
    ros::init(argc, argv, "exploration_manager_node");
    ros::NodeHandle nh_("");
    ros::NodeHandle nh_private_("~");

    EXPLORATION_MANAGER exploration_manager(nh_, nh_private_);

    exploration_manager.init();

    return 0;
}