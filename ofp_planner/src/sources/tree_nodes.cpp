#include "tree_nodes.h"

NODE::NODE(int INDEX, Eigen::Vector3d XYZ, visualization_msgs::MarkerArray* EDGES)
	: index(INDEX), xyz(XYZ), edges_marker(EDGES){
    parent = NULL;
    data.distance_travelled = 0.0;
    //ROS_INFO("Node: [%d]th Created", index);
}


NODE::NODE(){
    index = -1;
    parent = NULL;
}


NODE::~NODE(){
    //ROS_INFO("Node: [%d]th Killed", index);
}



bool NODE::operator()(const NODE* node) const{
	return this -> index == node -> index && this -> xyz == node -> xyz;
}



Eigen::Vector3d NODE::get_node_xyz(){
	return xyz;
}



int NODE::get_node_index(){
  	return index;
}



int NODE::get_num_childs(){
  	return childs.size();
}



void NODE::adopt_child(NODE* new_child){
    if(new_child -> parent == NULL){
        childs.push_back(new_child);
        new_child -> parent = this;
        //ROS_INFO("Node: [%d]th adopted node: [%d]th. Childs: %d", get_node_index(), new_child -> get_node_index(), get_num_childs());
        rviz_load_edge(this, new_child);
    }

    else if(new_child -> parent == this){
        return;
    }

    else{
        /*ROS_INFO("Node: [%d]th wants to adopt node: [%d]th but its child of node: [%d]th. RECONNECTING", 
                get_node_index(), new_child -> get_node_index(), new_child -> parent -> get_node_index());*/
        reject_child(new_child -> parent, new_child);
        adopt_child(new_child);
    }    
}



void NODE::reject_child(NODE* old_parent, NODE* old_child){
    vector<NODE*>::iterator it;
    int ith;

    it = find_if(old_parent -> childs.begin(),old_parent ->  childs.end(), *old_child);

    if(it != old_parent -> childs.end()){
        rviz_delete_marker(old_child -> get_node_index());
        ith = distance(old_parent -> childs.begin(), it);
        old_parent -> childs.erase(old_parent -> childs.begin() + ith);
        old_child -> parent = NULL;
        /*ROS_INFO("Node: [%d]th rejected node: [%d]th. Childs: %d", old_parent -> get_node_index(), old_child -> get_node_index(), 
                old_parent -> get_num_childs());*/
    }
}



void NODE::rviz_load_node(){ // Function to print new edge when publishing the final topic
    geometry_msgs::Point position;
    visualization_msgs::Marker point;

    point.header.frame_id = "world";
    point.header.stamp = ros::Time();
    point.ns = "fast_rrt_star_planner_nodes";
    point.action = visualization_msgs::Marker::ADD;
    point.id = this -> get_node_index();
    point.lifetime = ros::Duration();
    point.type = visualization_msgs::Marker::SPHERE_LIST;
    point.scale.x = point.scale.y = point.scale.z = 0.1;
    point.color.a = 1.0;
    point. color.r = 0.0;
    point.color.g = 1.0;
    point.color.b = 0.0;
    point.pose.orientation.w = 1.0;

	position.x = this -> get_node_xyz().x();
	position.y = this -> get_node_xyz().y();
	position.z = this -> get_node_xyz().z();

	point.points.push_back(position); // Saving point 1

    this -> edges_marker -> markers.push_back(point); 
}



void NODE::rviz_load_edge(NODE* node1, NODE* node2){ // Function to print new edge when publishing the final topic
    geometry_msgs::Point xyz_1, xyz_2;
    visualization_msgs::Marker edge;

    edge.header.frame_id = "world";
    edge.header.stamp = ros::Time();
    edge.ns = "fast_rrt_star_planner_edges";
    edge.action = visualization_msgs::Marker::ADD;
    edge.id = node2 -> get_node_index();
    edge.lifetime = ros::Duration();
    edge.type = visualization_msgs::Marker::ARROW;
    edge.scale.x = 0.03;
	edge.scale.y = 0.15;
	edge.scale.z = 0.025;
    edge.color.a = 1.0;
    edge. color.r = 0.0;
    edge.color.g = 1.0;
    edge.color.b = 0.0;
    edge.pose.orientation.w = 1.0;

	xyz_1.x = node1 -> get_node_xyz().x();
	xyz_1.y = node1 -> get_node_xyz().y();
	xyz_1.z = node1 -> get_node_xyz().z();

	xyz_2.x = node2 -> get_node_xyz().x();
	xyz_2.y = node2 -> get_node_xyz().y();
	xyz_2.z = node2 -> get_node_xyz().z();

	edge.points.push_back(xyz_1); // Saving point 1
    edge.points.push_back(xyz_2); // Saving point 2 because a edge needs to store both conected points consecutive
    this -> edges_marker -> markers.push_back(edge); 
}




bool NODE::rviz_delete_marker(int marker_id){
    int ith = 0;
    for(auto ptr = this -> edges_marker -> markers.begin() ; ptr != this -> edges_marker -> markers.end() ; ++ptr){
        
        if(ptr -> id == marker_id){
            this -> edges_marker -> markers.erase((this -> edges_marker -> markers.begin()) + ith);
            return true;
        }
        ith++;
    }
    return false;
    ROS_INFO("Could not find specific marker to delete");
}