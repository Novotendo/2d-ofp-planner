#ifndef FRONTIERS_FILTER_H
#define FRONTIERS_FILTER_H

#include <ros/ros.h>
#include <planning_lib.h>


class FRONTIERS_FILTER {

    public:
        FRONTIERS_FILTER(const ros::NodeHandle& nh, const ros::NodeHandle& nh_private);
        virtual ~FRONTIERS_FILTER() {}

        bool is_explored(const Eigen::Vector3d& frontier) const;

        void init();
        void rviz_show_filtered_frontiers();
        void clustering_filter(Eigen::Vector3d& point);
        void update_frontier_points(vector<Eigen::Vector3d>& frontier_points);
        void map_callback(const nav_msgs::OccupancyGrid::ConstPtr& msg);
        void global_frontier_callback(const geometry_msgs::Point::ConstPtr& msg);

        vector<int8_t> info_in_area(const Eigen::Vector3d& center, double& radius) const;

        space_state check_space_state(const Eigen::Vector3d& position, const double& radius) const;
        unsigned int get_index_of_point(const Eigen::Vector3d& point) const;
        Eigen::Vector3d get_point_of_index(const unsigned int& index) const;

    private:
        ros::NodeHandle nh_;
        ros::NodeHandle nh_private_;

        // Publishers
        ros::Publisher rviz_filtered_frontier_points_pub, filtered_frontier_points_pub;
        ros::Subscriber map_sub, global_frontiers_points_sub;

        // Map
        nav_msgs::OccupancyGrid mapData;

        // Detector stuff
        vector<Eigen::Vector3d> filtered_frontier_points;
        Eigen::Vector3d new_global_frontier_point;
        double robot_radius, resolution, clustering_radius, free_th;
        double map_width, map_height, map_x_init, map_y_init, map_resolution;
};

#endif