#ifndef GLOBAL_FRONTIER_DETECTOR_H
#define GLOBAL_FRONTIER_DETECTOR_H

#include <ros/ros.h>
#include <planning_lib.h>
#include <tree_nodes.h>

class GLOBAL_FRONTIER_DETECTOR {

    public:
        GLOBAL_FRONTIER_DETECTOR(const ros::NodeHandle& nh, const ros::NodeHandle& nh_private);
        virtual ~GLOBAL_FRONTIER_DETECTOR() {}

        bool is_explored(const Eigen::Vector3d& frontier) const;

        double find_nearest_node(Eigen::Vector3d& q, NODE& q_nearest);
        double get_node2node_distance(NODE& q1, NODE& q2);

        void compute_map_bounds(Eigen::Vector2d& lower_bound, Eigen::Vector2d& upper_bound);
        void generate_random_point(Eigen::Vector3d& random_point) const;
        void init();
        void expand_tree(Eigen::Vector3d& x_rand, NODE& x_near);
        void rviz_show_frontiers();
        void update_frontier_points(vector<Eigen::Vector3d>& frontier_points);
        void map_callback(const nav_msgs::OccupancyGrid::ConstPtr& msg);

        space_state check_space_state(const Eigen::Vector3d& position, const double& radius) const;
        unsigned int get_index_of_point(const Eigen::Vector3d& point) const;
        Eigen::Vector3d get_point_of_index(const unsigned int& index) const;

    private:
        ros::NodeHandle nh_;
        ros::NodeHandle nh_private_;

        // Publishers
        ros::Publisher rviz_tree_pub, rviz_frontier_points_pub, frontier_points_pub;
        ros::Subscriber map_sub;

        // Map
        nav_msgs::OccupancyGrid mapData;

        // Bounds on the size of the map
        Eigen::Vector2d map_lower_bound;
        Eigen::Vector2d map_upper_bound;

        // Detector stuff
        vector<NODE*> TREE;
        vector<Eigen::Vector3d> frontier_points;
        Eigen::Vector3d random_position;
        Eigen::Vector3d SEED;
	    NODE q_nearest;
        visualization_msgs::MarkerArray tree_edges;
        double robot_radius, resolution, free_th;
        double map_width, map_height, map_x_init, map_y_init, map_resolution;
};

#endif