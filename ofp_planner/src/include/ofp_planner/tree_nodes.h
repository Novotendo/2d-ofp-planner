#ifndef TREE_NODES_H
#define TREE_NODES_H
#include "planning_lib.h"



struct data{
    double distance_travelled;
};



class NODE{ // Node object that stores its position, parent and childs
     public: // This is accesible within the program
        NODE(int INDEX, Eigen::Vector3d XYZ, visualization_msgs::MarkerArray* EGDES);
        NODE();
        virtual ~NODE();
        bool operator()(const NODE* node) const;

        NODE* parent;  
        vector<NODE*> childs; // Node pointers to childs vector, so changes in childs will be update (because its referencing)

        struct data data;

        Eigen::Vector3d get_node_xyz();
        int get_node_index();
        int get_num_childs();
        void adopt_child(NODE *new_child);
        void reject_child(NODE* old_parent, NODE* old_child);
        void rviz_load_node();
        void rviz_load_edge(NODE* node1, NODE* node2);
        bool rviz_delete_marker(int marker_id);

    private: // Available to this class and friend functions (if you try "my_node.x" within the program, it will give you an error)        
        int index; // Node index
        int num_childs; // Number of node childs
        Eigen::Vector3d xyz; // Node Position
        visualization_msgs::MarkerArray* edges_marker;
};


#endif