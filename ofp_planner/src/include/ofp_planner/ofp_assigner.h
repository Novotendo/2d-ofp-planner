#ifndef OFP_ASSIGNER_H
#define OFP_ASSIGNER_H

#include <ros/ros.h>
#include <planning_lib.h>


struct OFP{
  Eigen::Vector3d position;
  double gain;
};



class OFP_ASSIGNER {

    public:
        OFP_ASSIGNER(const ros::NodeHandle& nh, const ros::NodeHandle& nh_private);
        virtual ~OFP_ASSIGNER() {}

        void init();
        void rviz_show_ofp();
        void robot_pose_callback(const geometry_msgs::PoseStamped::ConstPtr& msg);
        void map_callback(const nav_msgs::OccupancyGrid::ConstPtr& msg);
        void frontiers_filter_callback(const ofp_planner::point_array::ConstPtr& msg);
        void compute_ofp(OFP& optimal_frontier_point);

        double info_in_area(const Eigen::Vector3d& center, double& radius) const;

        space_state check_space_state(const Eigen::Vector3d& position) const;
        unsigned int get_index_of_point(const Eigen::Vector3d& point) const;
        Eigen::Vector3d get_point_of_index(const unsigned int& index) const;

    private:
        ros::NodeHandle nh_;
        ros::NodeHandle nh_private_;

        // Publishers
        ros::Publisher rviz_optimal_frontier_point_pub, optimal_frontier_point_pub, finish_exploration_pub;
        ros::Subscriber robot_pose_sub, map_sub, filtered_frontiers_points_sub;

        // Map
        nav_msgs::OccupancyGrid mapData;

        // Detector stuff
        vector<Eigen::Vector3d> filtered_frontier_points, ofp_candidates_list;
        Eigen::Vector3d new_global_frontier_point, robot_pose;
        double robot_radius, resolution, information_radius, free_th, lambda_path_cost;
        double map_width, map_height, map_x_init, map_y_init, map_resolution;
        bool pose_updated, map_updated;

        OFP optimal_frontier_point;

        ros::WallDuration wall_reset_duration;
	      ros::WallTime wall_reset_begin, wall_reset_now;
};

#endif