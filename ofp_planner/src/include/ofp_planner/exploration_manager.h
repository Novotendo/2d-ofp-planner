#ifndef EXPLORATION_MANAGER_H
#define EXPLORATION_MANAGER_H

#include <planning_lib.h>

class EXPLORATION_MANAGER{
    public:
        EXPLORATION_MANAGER(const ros::NodeHandle& nh, const ros::NodeHandle& nh_private);
        virtual ~EXPLORATION_MANAGER(){}

        bool mav_navigating, return_home, set_up, mav_pose_received;
        double exploration_height, robot_radius;
        geometry_msgs::PointStamped point2explore;

        void init_exploration_callback(const std_msgs::Bool& msg);
        void finish_exploration_callback(const std_msgs::Bool& msg);
        void explore_point_callback(const geometry_msgs::Point::ConstPtr& msg);
        void mav_pose_callback(const geometry_msgs::PoseStamped::ConstPtr& msg);
        void mav_take_off(const double& height) const;
        void mav_land() const;
        void mav_check_surroundings(const double& yaw) const;
        void init();
        bool mav_goal_arrived() const;


    private:   
        ros::NodeHandle nh_;
        ros::NodeHandle nh_private_;
        ros::Publisher mav_pose_pub, planner_request_pub;
        ros::Subscriber mav_pose_sub, init_exploration_sub, finish_exploration_sub, explore_point_sub;   

        Eigen::Vector4d mav_current_pose;
        Eigen::Vector3d home_position;
        tf::Quaternion mav_current_quaternion;

        int systemRet;
};
#endif