#ifndef PLANNING_LIB_H
#define PLANNING_LIB_H

#include <ros/ros.h>
#include <vector>
#include <eigen3/Eigen/Dense>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/PointStamped.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>
#include <algorithm>
#include <iostream>
#include <vector>
#include <cmath>
#include <rviz_visual_tools/rviz_visual_tools.h>
#include <filters/filter_chain.h>
#include <nav_msgs/OccupancyGrid.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/PoseStamped.h>
#include <string>
#include <math.h> 
#include <stdio.h> 
#include <std_msgs/Bool.h>
#include <tf/transform_listener.h>

#include <mav_trajectory_generation/polynomial_optimization_linear.h>
#include <mav_trajectory_generation/trajectory.h>
#include <mav_trajectory_generation_ros/ros_visualization.h>
#include <mav_trajectory_generation_ros/trajectory_sampler_node.h>
#include <tf/tf.h>
#include <tf2/LinearMath/Quaternion.h>
#include "sensor_msgs/LaserScan.h"

#include "ofp_planner/point_array.h"


using namespace std;
using namespace ros;

enum space_state {FREE, OCCUPIED, UNEXPLORED};

double get_point2point_distance(Eigen::Vector3d q1, Eigen::Vector3d q2);
vector<Eigen::Vector3d> increase_resolution(Eigen::Vector3d q0, Eigen::Vector3d q1, int interpolator);



#endif