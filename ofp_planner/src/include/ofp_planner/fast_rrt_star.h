#ifndef FAST_RRT_STAR_H
#define FAST_RRT_STAR_H
#include "planning_lib.h"
#include "tree_nodes.h"



class FAST_RRT_STAR_PLANNER{
    public:
        FAST_RRT_STAR_PLANNER(const ros::NodeHandle& nh, const ros::NodeHandle& nh_private);
        virtual ~FAST_RRT_STAR_PLANNER(){}
        double resolution;
        double robot_radius;
        double exploration_height;
        double vel_max = 2.0;
        double accel_max = 2.0;
        unsigned int patience = 1;
        double map_width, map_height, map_x_init, map_y_init, map_resolution;
        double neighbours_radius;
        const double free_th = 0.40;
        const int dimension = 4;
        const int derivative_to_optimize = mav_trajectory_generation::derivative_order::SNAP;

        Eigen::Vector3d SEED, GOAL;
        vector<NODE*> TREE;
        vector<NODE*> OPTIMAL_PATH;
        vector<Eigen::Vector3d> MAV_PATH2TRACK;

        bool new_root, planner_working, init_exploration, plan_request;
        vector<Eigen::Vector3d> q_increased;
        Eigen::Vector3d random_position;
        NODE q_nearest;  
        NODE* goal_node;     

        Eigen::Vector4d mav_current_pose;
        tf::Quaternion mav_current_quaternion;
        // Bounds on the size of the map
        Eigen::Vector2d map_lower_bound;
        Eigen::Vector2d map_upper_bound;

        nav_msgs::OccupancyGrid mapData;

        visualization_msgs::MarkerArray init_goal, tree_nodes, tree_edges, tree_optimal_path, whole_path;

        bool check_if_goal_found(NODE* q);
        bool init_tree();
        void delete_tree();
        bool check_if_free_space(Eigen::Vector3d position);
        bool mav_goal_arrived() const;

        double get_node2node_distance(NODE* q1, NODE* q2);
        double find_nearest_node(Eigen::Vector3d& q, NODE& q_nearest);

        void generate_random_point(Eigen::Vector3d& random_point) const;
        void reconnect(NODE* node);
        void load_optimal_path2seed(NODE* node);
        void exploration_init_callback(const std_msgs::Bool& msg);
        void exploration_finish_callback(const std_msgs::Bool& msg);
        void tree_init_callback(const geometry_msgs::PointStamped& msg);
        void rrt_exploration_callback(const geometry_msgs::PointStamped& msg);
        void map_callback(const nav_msgs::OccupancyGrid::ConstPtr& msg);
        void mav_pose_callback(const geometry_msgs::PoseStamped::ConstPtr& msg);
        void compute_map_bounds(Eigen::Vector2d& lower_bound, Eigen::Vector2d& upper_bound);
        void expand_tree(Eigen::Vector3d& x_rand, NODE& x_near);
        void init();
        void reset();
        void mav_hover();

        void vertex4trajectory(mav_trajectory_generation::Vertex::Vector& vertices, vector<Eigen::Vector3d>& path, double& init_yaw, double& final_yaw);
        void solve_linear_trajectory(mav_trajectory_generation::Vertex::Vector& vertices, mav_trajectory_generation::Trajectory &trajectory, double v_max, double a_max);
        void sample_trajectory(mav_trajectory_generation::Trajectory& trajectory, trajectory_msgs::MultiDOFJointTrajectory& sampled_trajectory);
        void build_3d_trajectory(trajectory_msgs::MultiDOFJointTrajectory& sampled_trajectory, double& init_yaw, double& final_yaw);
        void mav_track_path(const vector<NODE*>& path2track);

        unsigned int get_index_of_point(const Eigen::Vector3d& point) const;

        vector<NODE*> find_neighbours(NODE* node);

        space_state check_space_state(const Eigen::Vector3d& position) const;


    private:   
        ros::NodeHandle nh_;
        ros::NodeHandle nh_private_;
        ros::Publisher rviz_tree_pub, rviz_optimal_path_pub, rviz_init_and_goal_pub, mav_pose_pub, mav_trajectory_pub;
        ros::Subscriber mav_pose_sub, map_sub, rviz_clicket_point_sub, request_plan_sub;   

        bool GOAL_FOUND;
};

#endif